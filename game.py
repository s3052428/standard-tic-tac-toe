def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(current_board, current_index):
    print_board(current_board)
    print("GAME OVER ", current_board[current_index], "has won")
    exit()


def is_row_winner(board, current_index):
    if board[current_index] == board[current_index + 1] and board[current_index + 1] == board[current_index + 2]:
        return True


def is_column_winner(board, current_index):
    if board[current_index] == board[current_index + 3] and board[current_index + 3] == board[current_index + 6]:
        return True


def is_diagonal_winner(board, current_index):
    if current_index == 2:
        if board[current_index] == board[current_index + 2] and board[current_index + 2] == board[current_index + 4]:
            return True
    elif current_index == 0:
        if board[current_index] == board[current_index + 4] and board[current_index + 4] == board[current_index + 8]:
            return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 0):
        game_over(board, 0)
    elif is_row_winner(board, 3):
        game_over(board, 3)
    elif is_row_winner(board, 7):
        game_over(board, 6)
    elif is_column_winner(board, 0):
        game_over(board, 0)
    elif is_column_winner(board, 1):
        game_over(board, 1)
    elif is_column_winner(board, 2):
        game_over(board, 2)
    elif is_diagonal_winner(board, 0):
        game_over(board, 0)
    elif is_diagonal_winner(board, 2):
        game_over(board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
